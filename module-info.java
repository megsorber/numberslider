module com.javafxcontrols.numberSlider {
    requires transitive javafx.base;
    requires transitive javafx.fxml;
    requires transitive javafx.controls;
    requires transitive javafx.graphics;
    requires transitive com.javafxcontrols.localizedNumberField;
    
    exports com.javafxcontrols.numberSlider;
    opens com.javafxcontrols.numberSlider;
}