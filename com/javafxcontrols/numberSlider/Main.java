package com.javafxcontrols.numberSlider;

import java.util.Locale;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;

public class Main extends Application {
	@Override
	public void start(Stage stage) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("NumberSliderExample.fxml"));
//			Parent root = loader.load();
//			Scene scene = new Scene(root);
//			stage.setTitle("Localized Number Slider demo");
//			stage.setScene(scene);
//			stage.setMaximized(true);
//			stage.show();
			
			double width = 600;
			VBox root = new VBox();
			
			double range1 = 10;
			String label1 = "Default localization:";
			NumberSlider numberSlider1 = new NumberSlider(label1, -range1, range1, 0.0, 3);
			
			double range2 = 10000000;
			String label2 = "German localization:";
			NumberSlider numberSlider2 = new NumberSlider(label2, -range2, range2, 0.0, Locale.GERMAN.toString());

			root.getChildren().add(numberSlider1);
			root.getChildren().add(numberSlider2);
			
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setTitle("NumberSlider Example");
			stage.setWidth(width);
			stage.setHeight(300);
			stage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
