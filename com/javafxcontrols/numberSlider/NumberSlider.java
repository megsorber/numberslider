package com.javafxcontrols.numberSlider;

import javafx.beans.NamedArg;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import com.javafxcontrols.localizedNumberField.LocalizedNumberField;

public class NumberSlider extends HBox  {
	
	private Label label;
	private Slider slider;
	private LocalizedNumberField lnf;
	private BooleanProperty isNumberFinal=new SimpleBooleanProperty(true); //Need a way to detect when the slider stops being moved
	
	public NumberSlider() {
		super();
		lnf = new LocalizedNumberField();
		initialize("label");
	}
	
	public NumberSlider(@NamedArg("label") String label) {
		super();
		lnf = new LocalizedNumberField();
		initialize(label);
	}
	
	public NumberSlider(@NamedArg("label") String label, @NamedArg("number") Double number) {
		super();
		lnf = new LocalizedNumberField(number);
		initialize(label);
	}
	
	public NumberSlider(@NamedArg("label") String label, @NamedArg("locale") String locale) {
		super();
		lnf = new LocalizedNumberField(locale);
		initialize(label);
	}
	
	public NumberSlider(@NamedArg("label") String label, @NamedArg("min") Double min, @NamedArg("max") Double max) {
		super();
		lnf = new LocalizedNumberField(min, max);
		initialize(label);
	}
	
	public NumberSlider(@NamedArg("label") String label, @NamedArg("min") Double min, @NamedArg("max") Double max, 
			@NamedArg("number") Double number) {
		super();
		lnf = new LocalizedNumberField(min, max, number);
		initialize(label);
	}
	
	public NumberSlider(@NamedArg("label") String label, @NamedArg("min") Double min, @NamedArg("max") Double max, 
			@NamedArg("number") Double number, @NamedArg("maxDecimals") int maxDecimals) {
		super();
		lnf = new LocalizedNumberField(min, max, number, maxDecimals);
		initialize(label);
	}
	
	public NumberSlider(@NamedArg("label") String label, @NamedArg("min") Double min, @NamedArg("max") Double max, 
			@NamedArg("number") Double number, @NamedArg("locale") String locale) {
		super();
		lnf = new LocalizedNumberField(min, max, number, locale);
		initialize(label);
	}
	
	public NumberSlider(@NamedArg("label") String label, @NamedArg("min") Double min, @NamedArg("max") Double max, 
			@NamedArg("number") Double number, @NamedArg("maxDecimals") int maxDecimals, 
			@NamedArg("locale") String locale) {
		super();
		lnf = new LocalizedNumberField(min, max, number, maxDecimals, locale);
		initialize(label);
	}
	
	private void initialize(String labelText) {
		label = new Label(labelText);
		slider = new Slider(lnf.getMin(), lnf.getMax(), lnf.getNumber());
		slider.setMajorTickUnit((lnf.getMax()-lnf.getMin())/10);

		slider.valueProperty().addListener(this::sliderChanged);
		lnf.numberProperty().addListener(this::numberChanged);
		
		this.getChildren().add(this.label);
		this.getChildren().add(this.slider);
		this.getChildren().add(this.lnf);
		
		slider.setOnMousePressed(this::setFinalFalse);
		slider.setOnMouseReleased(this::setFinalTrue);
	}
	
	private void setFinalFalse(MouseEvent me) {
		isNumberFinal.set(false);
	}
	
	private void setFinalTrue(MouseEvent me) {
		isNumberFinal.set(true);
	}
	
	public BooleanProperty getIsNumberFinal() {
		return isNumberFinal;
	}
	
	private void sliderChanged(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
		lnf.setNumber((double) newValue);
	}
	
	private void numberChanged(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
		//check if the number changed due to the slider changing, if so don't touch anything
		//since javaFX will sometimes throw an error
		if(slider.getValue() != newValue.doubleValue() && newValue.doubleValue() != Double.NaN) {
			slider.setValue(newValue.doubleValue());
		}
	}
	
	public Label getLabel() {
		return label;
	}
	public void setLabel(Label label) {
		this.label = label;
	}

	public double getMin() {
		return slider.getMin();
	}
	public void setMin(double min) {
		//error checking happens in the localizednumberfield
		lnf.setMin(min);
		slider.setMin(min);
	}

	public double getMax() {
		return slider.getMax();
	}
	public void setMax(double max) {
		//error checking happens in the localizednumberfield
		lnf.setMax(max);
		slider.setMax(max);
	}

	public int getMaxDecimals() {
		return lnf.getMaxDecimals();
	}
	public void setMaxDecimals(int maxDecimals) {
		lnf.setMaxDecimals(maxDecimals);
	}

	public Double getNumberProperty() {
		return lnf.getNumber();
	}
	public DoubleProperty numberProperty() {
		return lnf.numberProperty();
	}
	
	public Slider getSlider() {
		return slider;
	}
	
	public LocalizedNumberField getLocalizedNumberField() {
		return lnf;
	}
}