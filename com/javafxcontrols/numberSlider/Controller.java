package com.javafxcontrols.numberSlider;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class Controller {
	@FXML protected NumberSlider exampleNS;
	@FXML protected Label exampleLabel;
	
	@FXML
	public void initialize() {
		exampleLabel.textProperty().bind(exampleNS.numberProperty().asString());
	}
}
